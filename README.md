# chuck-norris-graphql

[GraphQl](http://facebook.github.io/graphql/October2016/) API that exposes jokes about Chuck Norris. It includes [GraphiQl](https://github.com/graphql/graphiql) for exploring the API.

## Getting started

The application is build using [TypeScript](https://www.typescriptlang.org/). Therefor you'll need [Node.js and npm](https://nodejs.org/en/) first, before proceeding with any of the steps below.

Running the application can be done by following the steps below.

1. Install NPM modules: `npm install`
2. Build the application: `npm run build`
3. Start the application: `npm start`

The `/graphql` endpoint is now up and running to receive POST requests. The in-browser IDE GraphiQl is reachable at `/graphiql` also.

### Developing

While developing you can use the commands below to automatically build and refresh upon a code change:

1. Install NPM modules: `npm install`
2. Start build in watch mode: `npm run watch`
3. Serve and reload upon changes: `npm run serve`

### NPM-run-scripts

Start the scripts below using: `npm run <command>`

* `start`: start the application after you've done a build.
* `test`: test your source code (linting only for now).
* `build`: build source code.
* `watch`: auto-compile TypeScript when making changes.
* `serve`: serve your compiled files during development and reload upon change.
* `lint`: lint your files
* `tsc`: run the TyeScript compiler from the local repository (and not globally installed). Add commands after a `--` like `npm run tsc -- --help`

- - - -

## User stories

### #1: Continuous testing

As a developer, I want to ensure our code adheres to our standards and can be built. Create a pipeline to automatically run testing and a build upon commit.

**Requirements:**

* Create a pipeline to lint and build changes upon a commit to the remote repository.

### #2: Continuous deployment to testing

As a developer, I want to test new features quickly. Create a pipeline to automatically deploy new feature to our testing environment.

**Requirements:**

* Create a pipeline to automatically deploy new versions of the API to our testing environment.
* Before deploying, ensure the code is linted and built a final time, to ensure the merged code is still proper.
* Do not commit any usernames or passwords to the repository.

### #3: Continuous deployment of staging & production

As a customer, I want new Chuck Norris jokes to be deployed to our production once they have been tested. Create a pipeline to automatically deploy the staging and production branches to their respective environments.

**Requirements:**

* Create a pipeline to automatically deploy new changes on the staging and production branches to their respective environments.
* Before deploying, ensure the code is linted and built a final time, to ensure the merged code is still proper.
* Do not commit any usernames or passwords to the repository.

### #4: Efficient automatic deployment

As a developer, I want to ensure my deployments are as efficient as possible. Modify the deployment pipeline to only deploy the built code, not everything contained in the repository.

**Requirements:**

* Modify the deployment pipeline to deploy only the content of the build (`/dist`) folder and the package.json file.

### #5: API improvements

As a customer, I want to keep expanding my API with new jokes. Add a Chuck Norris joke to the API.

**Requirements:**

* Find a worthy Chuck Norris joke and add it to the API.
* Add the proper author of the joke.
